-- USE [master]
-- GO
-- /****** Object:  Database [ITSCADA]    Script Date: 2/15/2020 4:18:18 PM ******/
-- CREATE DATABASE [ITSCADA]
--  CONTAINMENT = NONE
--  ON  PRIMARY 
-- ( NAME = N'ITSCADA', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\ITSCADA.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
--  LOG ON 
-- ( NAME = N'ITSCADA_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\ITSCADA_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
-- GO
-- ALTER DATABASE [ITSCADA] SET COMPATIBILITY_LEVEL = 140
-- GO
-- IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
-- begin
-- EXEC [ITSCADA].[dbo].[sp_fulltext_database] @action = 'enable'
-- end
-- GO
-- ALTER DATABASE [ITSCADA] SET ANSI_NULL_DEFAULT OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET ANSI_NULLS OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET ANSI_PADDING OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET ANSI_WARNINGS OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET ARITHABORT OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET AUTO_CLOSE OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET AUTO_SHRINK OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET AUTO_UPDATE_STATISTICS ON 
-- GO
-- ALTER DATABASE [ITSCADA] SET CURSOR_CLOSE_ON_COMMIT OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET CURSOR_DEFAULT  GLOBAL 
-- GO
-- ALTER DATABASE [ITSCADA] SET CONCAT_NULL_YIELDS_NULL OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET NUMERIC_ROUNDABORT OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET QUOTED_IDENTIFIER OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET RECURSIVE_TRIGGERS OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET  DISABLE_BROKER 
-- GO
-- ALTER DATABASE [ITSCADA] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET DATE_CORRELATION_OPTIMIZATION OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET TRUSTWORTHY OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET ALLOW_SNAPSHOT_ISOLATION OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET PARAMETERIZATION SIMPLE 
-- GO
-- ALTER DATABASE [ITSCADA] SET READ_COMMITTED_SNAPSHOT OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET HONOR_BROKER_PRIORITY OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET RECOVERY FULL 
-- GO
-- ALTER DATABASE [ITSCADA] SET  MULTI_USER 
-- GO
-- ALTER DATABASE [ITSCADA] SET PAGE_VERIFY CHECKSUM  
-- GO
-- ALTER DATABASE [ITSCADA] SET DB_CHAINING OFF 
-- GO
-- ALTER DATABASE [ITSCADA] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
-- GO
-- ALTER DATABASE [ITSCADA] SET TARGET_RECOVERY_TIME = 60 SECONDS 
-- GO
-- ALTER DATABASE [ITSCADA] SET DELAYED_DURABILITY = DISABLED 
-- GO
-- EXEC sys.sp_db_vardecimal_storage_format N'ITSCADA', N'ON'
-- GO
-- ALTER DATABASE [ITSCADA] SET QUERY_STORE = OFF
-- GO
USE [ITSCADA]
GO
/****** Object:  Table [dbo].[BwActionTable]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BwActionTable](
	[ProjNodeId] [int] NOT NULL,
	[LogDate] [varchar](12) NOT NULL,
	[LogTime] [varchar](12) NOT NULL,
	[Priority] [varchar](5) NULL,
	[TagName] [varchar](32) NOT NULL,
	[Description] [varchar](65) NULL,
	[Action] [varchar](80) NULL,
	[UserName] [varchar](32) NULL,
	[NodeName] [varchar](32) NULL,
	[NodeIP] [varchar](32) NULL,
 CONSTRAINT [pk_ActionTable_1] PRIMARY KEY CLUSTERED 
(
	[ProjNodeId] ASC,
	[LogDate] ASC,
	[LogTime] ASC,
	[TagName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BwAlarmTable]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BwAlarmTable](
	[ProjNodeId] [int] NOT NULL,
	[LogDate] [varchar](12) NOT NULL,
	[LogTime] [varchar](12) NOT NULL,
	[Priority] [varchar](5) NULL,
	[TagName] [varchar](32) NOT NULL,
	[Description] [varchar](65) NULL,
	[Action] [varchar](80) NULL,
	[UserName] [varchar](32) NULL,
	[NodeName] [varchar](32) NULL,
	[NodeIP] [varchar](32) NULL,
	[AlmGroup] [varchar](5) NULL,
 CONSTRAINT [pk_AlarmTable_1] PRIMARY KEY CLUSTERED 
(
	[ProjNodeId] ASC,
	[LogDate] ASC,
	[LogTime] ASC,
	[TagName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BwAlarmTableValue]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BwAlarmTableValue](
	[ProjNodeId] [int] NOT NULL,
	[LogDate] [varchar](12) NOT NULL,
	[LogTime] [varchar](12) NOT NULL,
	[Priority] [varchar](5) NULL,
	[TagName] [varchar](32) NOT NULL,
	[Description] [varchar](65) NULL,
	[Action] [varchar](80) NULL,
	[UserName] [varchar](32) NULL,
	[NodeName] [varchar](32) NULL,
	[NodeIP] [varchar](32) NULL,
	[AlmGroup] [varchar](5) NULL,
	[AlarmValue] [numeric](18, 6) NULL,
	[AlarmLimit] [numeric](18, 6) NULL,
 CONSTRAINT [pk_AlarmTable_XX] PRIMARY KEY CLUSTERED 
(
	[ProjNodeId] ASC,
	[LogDate] ASC,
	[LogTime] ASC,
	[TagName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BwAnaChgLog]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BwAnaChgLog](
	[ProjNodeId] [int] NOT NULL,
	[TagName] [varchar](32) NOT NULL,
	[LogDate] [varchar](12) NOT NULL,
	[LogTime] [varchar](12) NOT NULL,
	[LogMilliSecond] [int] NOT NULL,
	[LogValue] [numeric](18, 6) NULL,
	[Alarm] [int] NULL,
 CONSTRAINT [PK_BwAnaChgLog] PRIMARY KEY CLUSTERED 
(
	[ProjNodeId] ASC,
	[TagName] ASC,
	[LogDate] ASC,
	[LogTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BwAnalogTable]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BwAnalogTable](
	[ProjNodeId] [int] NOT NULL,
	[TagName] [varchar](32) NOT NULL,
	[LogDate] [varchar](12) NOT NULL,
	[LogTime] [varchar](12) NOT NULL,
	[MaxValue] [numeric](18, 6) NULL,
	[AvgValue] [numeric](18, 6) NULL,
	[MinValue] [numeric](18, 6) NULL,
	[LastValue] [numeric](18, 6) NULL,
	[Alarm] [int] NULL,
 CONSTRAINT [PK_BwAnalogTable_1] PRIMARY KEY CLUSTERED 
(
	[ProjNodeId] ASC,
	[TagName] ASC,
	[LogDate] ASC,
	[LogTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BwDiscreteTable]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BwDiscreteTable](
	[ProjNodeId] [int] NOT NULL,
	[TagName] [varchar](32) NOT NULL,
	[LogDate] [varchar](12) NOT NULL,
	[LogTime] [varchar](12) NOT NULL,
	[LogMilliSecond] [int] NOT NULL,
	[LogValue] [int] NULL,
	[Alarm] [int] NULL,
 CONSTRAINT [pk_DiscreteTable_1] PRIMARY KEY CLUSTERED 
(
	[ProjNodeId] ASC,
	[LogDate] ASC,
	[LogTime] ASC,
	[TagName] ASC,
	[LogMilliSecond] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BwLogTagExt]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BwLogTagExt](
	[ProjNodeId] [int] NOT NULL,
	[TagName] [varchar](32) NOT NULL,
	[TagType] [int] NOT NULL,
	[ProjId] [int] NOT NULL,
 CONSTRAINT [pk_BwLogTagExt_1] PRIMARY KEY CLUSTERED 
(
	[ProjNodeId] ASC,
	[TagName] ASC,
	[TagType] ASC,
	[ProjId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BwLogTblInfo]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BwLogTblInfo](
	[ProjIdbw] [int] NULL,
	[ProjNodeIdbw] [int] NULL,
	[LogTableId] [int] NULL,
	[ArchDate] [varchar](12) NULL,
	[DeleteDate] [varchar](12) NULL,
	[RestoreDate] [varchar](12) NULL,
	[ActionType] [int] NULL,
	[Action] [varchar](80) NULL,
	[LogData1] [int] NULL,
	[LogData2] [int] NULL,
	[LogText1] [varchar](64) NULL,
	[LogText2] [varchar](64) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BwSysLogTable]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BwSysLogTable](
	[ProjNodeId] [int] NULL,
	[LogDate] [varchar](12) NULL,
	[LogTime] [varchar](12) NULL,
	[UserName] [varchar](32) NULL,
	[ActionType] [int] NULL,
	[Action] [varchar](255) NULL,
	[SourcePC] [varchar](32) NULL,
	[SourceIP] [varchar](32) NULL,
	[ProjId] [int] NULL,
	[ProjName] [varchar](32) NULL,
	[NodeName] [varchar](32) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BwTextTable]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BwTextTable](
	[ProjNodeId] [int] NOT NULL,
	[TagName] [varchar](32) NOT NULL,
	[LogDate] [varchar](12) NOT NULL,
	[LogTime] [varchar](12) NOT NULL,
	[TextData] [varchar](80) NULL,
 CONSTRAINT [pk_TextTable_1] PRIMARY KEY CLUSTERED 
(
	[ProjNodeId] ASC,
	[LogDate] ASC,
	[LogTime] ASC,
	[TagName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ConfigTable]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConfigTable](
	[no] [int] IDENTITY(1,1) NOT NULL,
	[production] [nvarchar](50) NULL,
	[line] [nvarchar](50) NULL,
	[tagname] [nvarchar](50) NULL,
	[io] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeploymentDisplay]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeploymentDisplay](
	[No] [int] IDENTITY(1,1) NOT NULL,
	[Station] [varchar](32) NULL,
	[FullName] [varchar](32) NULL,
	[Picture] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Downtime]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Downtime](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[caseid] [nvarchar](50) NULL,
	[date] [date] NULL,
	[project] [nvarchar](30) NULL,
	[line] [nvarchar](30) NULL,
	[failure_cat1] [nvarchar](50) NULL,
	[failure_cat2] [nvarchar](50) NULL,
	[failure_cat3] [nvarchar](50) NULL,
	[timedown] [datetime] NULL,
	[loggedby] [nvarchar](50) NULL,
	[timeup] [datetime] NULL,
	[timeduration] [int] NULL,
	[serviceby] [nvarchar](50) NULL,
	[repair_cat1] [nvarchar](50) NULL,
	[repair_cat2] [nvarchar](50) NULL,
	[repair_cat3] [nvarchar](50) NULL,
	[pinchange] [nchar](10) NULL,
	[remarks] [nvarchar](max) NULL,
	[status] [nvarchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ESD_Current_Status]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ESD_Current_Status](
	[Combine_ID] [varchar](32) NOT NULL,
	[Log_Value] [int] NULL,
	[Update_Time] [datetime] NULL,
 CONSTRAINT [pk_ESD_Current_Status] PRIMARY KEY CLUSTERED 
(
	[Combine_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Failure_cat]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Failure_cat](
	[no] [int] NOT NULL,
	[line] [nvarchar](20) NULL,
	[failure_cat1] [nvarchar](50) NULL,
	[failure_cat2] [nvarchar](50) NULL,
	[failure_cat3] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Line]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Line](
	[no] [int] NULL,
	[Block] [nchar](10) NULL,
	[Floor] [nchar](10) NULL,
	[Line] [nchar](30) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Machine_Availability_Today]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Machine_Availability_Today](
	[Record_ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [varchar](12) NOT NULL,
	[Combine_ID] [varchar](32) NOT NULL,
	[Total_Off_Time] [int] NULL,
	[Total_Run_Time] [int] NULL,
	[Total_Down_Time] [int] NULL,
	[Total_Idle_Time] [int] NULL,
	[Total_Down_Occurrence] [int] NULL,
	[Total_Input_Time] [int] NULL,
	[Availability] [decimal](5, 2) NULL,
	[Update_Time] [datetime] NULL,
 CONSTRAINT [pk_Machine_Availability_Today] PRIMARY KEY CLUSTERED 
(
	[Record_ID] ASC,
	[Combine_ID] ASC,
	[Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Machine_Availability_Trend_Today]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Machine_Availability_Trend_Today](
	[Record_ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [varchar](12) NOT NULL,
	[Time] [varchar](12) NOT NULL,
	[Combine_ID] [varchar](32) NOT NULL,
	[Availability] [decimal](5, 2) NULL,
	[Update_Time] [datetime] NULL,
 CONSTRAINT [pk_Machine_Availability_Trend_Today] PRIMARY KEY CLUSTERED 
(
	[Record_ID] ASC,
	[Combine_ID] ASC,
	[Date] ASC,
	[Time] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Machine_Current_Status]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Machine_Current_Status](
	[Combine_ID] [varchar](32) NOT NULL,
	[Log_Value] [numeric](18, 6) NULL,
	[Update_Time] [datetime] NULL,
 CONSTRAINT [pk_Machine_Current_Status] PRIMARY KEY CLUSTERED 
(
	[Combine_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Machine_Info]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Machine_Info](
	[Combine_ID] [varchar](32) NOT NULL,
	[Factory_ID] [varchar](10) NULL,
	[Area_ID] [varchar](10) NULL,
	[Line_ID] [varchar](10) NULL,
	[Machine_ID] [varchar](10) NULL,
	[Machine_Name] [varchar](64) NULL,
	[Pic_URL] [varchar](256) NULL,
	[Info] [varchar](256) NULL,
	[Update_Time] [datetime] NULL,
 CONSTRAINT [pk_Machine_Info] PRIMARY KEY CLUSTERED 
(
	[Combine_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Machine_Log_Raw]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Machine_Log_Raw](
	[Record_ID] [int] IDENTITY(1,1) NOT NULL,
	[Combine_ID] [varchar](32) NOT NULL,
	[ProNode_ID] [int] NULL,
	[Date] [varchar](12) NOT NULL,
	[Tag_Name] [varchar](32) NULL,
	[Start_Time] [datetime] NULL,
	[End_Time] [datetime] NULL,
	[Log_Value] [int] NULL,
	[Duration] [int] NULL,
 CONSTRAINT [pk_Machine_Log_Raw] PRIMARY KEY CLUSTERED 
(
	[Record_ID] ASC,
	[Combine_ID] ASC,
	[Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Machine_Log_Today]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Machine_Log_Today](
	[Record_ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [varchar](12) NOT NULL,
	[Combine_ID] [varchar](32) NOT NULL,
	[Start_Time] [datetime] NULL,
	[End_Time] [datetime] NULL,
	[Log_Value] [int] NULL,
	[Duration] [int] NULL,
	[Update_Time] [datetime] NULL,
 CONSTRAINT [pk_Machine_Log_Today] PRIMARY KEY CLUSTERED 
(
	[Record_ID] ASC,
	[Combine_ID] ASC,
	[Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ManPowerDisplay]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ManPowerDisplay](
	[No] [int] IDENTITY(1,1) NOT NULL,
	[Date] [nvarchar](50) NULL,
	[Login] [nvarchar](50) NULL,
	[Logout] [nvarchar](50) NULL,
	[Station] [char](30) NULL,
	[Line] [char](30) NULL,
	[UserName] [char](30) NULL,
	[FullName] [char](30) NULL,
	[Picture] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterList]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterList](
	[no] [int] IDENTITY(1,1) NOT NULL,
	[product_no] [nvarchar](50) NULL,
	[boardside] [nchar](10) NULL,
	[boardcavity] [int] NULL,
	[line] [nchar](30) NULL,
	[cycletime] [nvarchar](10) NULL,
	[lastupdate] [datetime] NULL,
	[updateby] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mechinebom]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mechinebom](
	[id] [int] NOT NULL,
	[project] [nvarchar](30) NULL,
	[line] [nvarchar](30) NULL,
	[station] [nvarchar](50) NULL,
	[parttype] [nvarchar](50) NULL,
	[partno] [nvarchar](200) NULL,
	[partdescription] [nvarchar](max) NULL,
	[qty] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pEventLogA]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pEventLogA](
	[ProjIdbw] [int] NULL,
	[ProjNodeIdbw] [int] NULL,
	[EventLogId] [int] NULL,
	[EventLogName] [varchar](64) NULL,
	[Description] [varchar](64) NULL,
	[TagMax] [int] NULL,
	[EveLogData1] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pEventTagA]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pEventTagA](
	[EventLogId] [int] NULL,
	[TagName] [varchar](32) NULL,
	[TagNbr] [int] NULL,
	[EveTagData1] [int] NULL,
	[EveTagData2] [int] NULL,
	[EveTagText1] [varchar](64) NULL,
	[EveTagText2] [varchar](64) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Project]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Project](
	[id] [int] NOT NULL,
	[projectname] [nvarchar](20) NULL,
	[line] [nvarchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Repair_cat]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Repair_cat](
	[no] [int] NOT NULL,
	[failure_cat1] [nvarchar](50) NULL,
	[repair_cat1] [nvarchar](50) NULL,
	[repair_cat2] [nvarchar](50) NULL,
	[repair_cat3] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Target]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Target](
	[no] [int] IDENTITY(1,1) NOT NULL,
	[product_no] [nvarchar](50) NULL,
	[boardside] [nvarchar](50) NULL,
	[boardcavity] [int] NULL,
	[cycletime] [decimal](18, 2) NULL,
	[production] [nvarchar](50) NULL,
	[line] [nvarchar](50) NULL,
	[line_status] [int] NULL,
	[start_by] [nvarchar](50) NULL,
	[start_date] [date] NULL,
	[start_time] [time](7) NULL,
	[am00] [int] NULL,
	[am01] [int] NULL,
	[am02] [int] NULL,
	[am03] [int] NULL,
	[am04] [int] NULL,
	[am05] [int] NULL,
	[am06] [int] NULL,
	[am07] [int] NULL,
	[am08] [int] NULL,
	[am09] [int] NULL,
	[am10] [int] NULL,
	[am11] [int] NULL,
	[pm12] [int] NULL,
	[pm13] [int] NULL,
	[pm14] [int] NULL,
	[pm15] [int] NULL,
	[pm16] [int] NULL,
	[pm17] [int] NULL,
	[pm18] [int] NULL,
	[pm19] [int] NULL,
	[pm20] [int] NULL,
	[pm21] [int] NULL,
	[pm22] [int] NULL,
	[pm23] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TargetTable]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TargetTable](
	[PN] [varchar](20) NOT NULL,
	[Model] [varchar](20) NOT NULL,
	[Line] [varchar](20) NOT NULL,
	[am7] [bigint] NOT NULL,
	[am8] [int] NOT NULL,
	[am9] [varchar](20) NOT NULL,
	[am10] [varchar](20) NOT NULL,
	[am11] [varchar](20) NOT NULL,
	[pm12] [varchar](20) NOT NULL,
	[pm1] [varchar](20) NOT NULL,
	[pm2] [varchar](20) NOT NULL,
	[pm3] [varchar](20) NOT NULL,
	[pm4] [varchar](20) NOT NULL,
	[pm5] [varchar](20) NOT NULL,
	[pm6] [varchar](20) NOT NULL,
	[pm7] [varchar](20) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Temperature]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Temperature](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TemperatureID] [nvarchar](50) NOT NULL,
	[TagName] [nvarchar](50) NOT NULL,
	[Area] [nvarchar](50) NOT NULL,
	[Offset] [real] NOT NULL,
	[HLimited] [nchar](10) NOT NULL,
	[LLimited] [nchar](10) NOT NULL,
	[UpdateDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Threshold]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Threshold](
	[no] [int] IDENTITY(1,1) NOT NULL,
	[line] [nvarchar](50) NULL,
	[area] [nvarchar](50) NULL,
	[Temperature_UL] [nchar](10) NULL,
	[Temperature_LL] [nchar](10) NULL,
	[Humidity_UL] [nchar](10) NULL,
	[Humidity_LL] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TrainingRecord]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrainingRecord](
	[no] [int] IDENTITY(1,1) NOT NULL,
	[employeename] [char](30) NULL,
	[datejoined] [nvarchar](50) NULL,
	[productline] [char](30) NULL,
	[supervisor] [char](30) NULL,
	[station] [char](30) NULL,
	[startdate] [nvarchar](50) NULL,
	[finishdate] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Userinform]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Userinform](
	[id] [int] NOT NULL,
	[userid] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[ulevel] [int] NULL,
	[username] [varchar](30) NULL,
	[employessid] [int] NULL,
	[email] [varchar](120) NULL,
	[picture] [image] NULL,
	[datejoined] [date] NULL,
	[remark] [char](200) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Userlogin]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Userlogin](
	[no] [int] IDENTITY(1,1) NOT NULL,
	[userid] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[ulevel] [char](10) NULL,
	[username] [char](30) NULL,
	[employessid] [varchar](max) NULL,
	[email] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Userowner]    Script Date: 2/15/2020 4:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Userowner](
	[no] [int] IDENTITY(1,1) NOT NULL,
	[userid] [nvarchar](50) NULL,
	[username] [nvarchar](50) NULL,
	[userowner] [nvarchar](50) NULL,
	[line] [nvarchar](50) NULL,
	[supervisor] [nvarchar](50) NULL,
	[stationtask] [nvarchar](50) NULL,
	[operator] [nvarchar](50) NULL
) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [ITSCADA] SET  READ_WRITE 
GO
