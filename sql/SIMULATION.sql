-- USE [master]
-- GO
-- /****** Object:  Database [SIMULATION]    Script Date: 2/15/2020 4:18:52 PM ******/
-- CREATE DATABASE [SIMULATION]
--  CONTAINMENT = NONE
--  ON  PRIMARY 
-- ( NAME = N'SIMULATION', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\SIMULATION.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
--  LOG ON 
-- ( NAME = N'SIMULATION_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\SIMULATION_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
-- GO
-- ALTER DATABASE [SIMULATION] SET COMPATIBILITY_LEVEL = 140
-- GO
-- IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
-- begin
-- EXEC [SIMULATION].[dbo].[sp_fulltext_database] @action = 'enable'
-- end
-- GO
-- ALTER DATABASE [SIMULATION] SET ANSI_NULL_DEFAULT OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET ANSI_NULLS OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET ANSI_PADDING OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET ANSI_WARNINGS OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET ARITHABORT OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET AUTO_CLOSE OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET AUTO_SHRINK OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET AUTO_UPDATE_STATISTICS ON 
-- GO
-- ALTER DATABASE [SIMULATION] SET CURSOR_CLOSE_ON_COMMIT OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET CURSOR_DEFAULT  GLOBAL 
-- GO
-- ALTER DATABASE [SIMULATION] SET CONCAT_NULL_YIELDS_NULL OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET NUMERIC_ROUNDABORT OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET QUOTED_IDENTIFIER OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET RECURSIVE_TRIGGERS OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET  DISABLE_BROKER 
-- GO
-- ALTER DATABASE [SIMULATION] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET DATE_CORRELATION_OPTIMIZATION OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET TRUSTWORTHY OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET ALLOW_SNAPSHOT_ISOLATION OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET PARAMETERIZATION SIMPLE 
-- GO
-- ALTER DATABASE [SIMULATION] SET READ_COMMITTED_SNAPSHOT OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET HONOR_BROKER_PRIORITY OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET RECOVERY FULL 
-- GO
-- ALTER DATABASE [SIMULATION] SET  MULTI_USER 
-- GO
-- ALTER DATABASE [SIMULATION] SET PAGE_VERIFY CHECKSUM  
-- GO
-- ALTER DATABASE [SIMULATION] SET DB_CHAINING OFF 
-- GO
-- ALTER DATABASE [SIMULATION] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
-- GO
-- ALTER DATABASE [SIMULATION] SET TARGET_RECOVERY_TIME = 60 SECONDS 
-- GO
-- ALTER DATABASE [SIMULATION] SET DELAYED_DURABILITY = DISABLED 
-- GO
-- EXEC sys.sp_db_vardecimal_storage_format N'SIMULATION', N'ON'
-- GO
-- ALTER DATABASE [SIMULATION] SET QUERY_STORE = OFF
-- GO
USE [SIMULATION]
GO
/****** Object:  Table [dbo].[BackendId]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BackendId](
	[BackendId] [int] NOT NULL,
	[PartId] [int] NULL,
	[ProcessName] [nvarchar](max) NULL,
	[Duration] [int] NULL,
 CONSTRAINT [PK_BackendId] PRIMARY KEY CLUSTERED 
(
	[BackendId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Belt]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Belt](
	[BeltId] [nchar](10) NOT NULL,
	[LineId] [nchar](10) NULL,
	[StatusCodeId] [nchar](10) NULL,
	[StatusCodeDescription] [nchar](10) NULL,
 CONSTRAINT [PK_Belt] PRIMARY KEY CLUSTERED 
(
	[BeltId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BwOpAnaChgLog]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BwOpAnaChgLog](
	[BwOpAnaChgLogId] [int] IDENTITY(1,1) NOT NULL,
	[ProjNodeId] [int] NOT NULL,
	[TagName] [varchar](32) NOT NULL,
	[LogDate] [varchar](12) NOT NULL,
	[LogTime] [varchar](12) NOT NULL,
	[LogMilliSecond] [int] NOT NULL,
	[LogValue] [numeric](18, 6) NULL,
	[Alarm] [int] NULL,
	[ProductNo] [nvarchar](50) NULL,
	[Production] [nvarchar](50) NULL,
	[Line] [nvarchar](50) NULL,
 CONSTRAINT [PK_BwOpAnaChgLog] PRIMARY KEY CLUSTERED 
(
	[BwOpAnaChgLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BwOpAnalogTable]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BwOpAnalogTable](
	[BwOpAnalogTableId] [int] IDENTITY(1,1) NOT NULL,
	[ProjNodeId] [int] NOT NULL,
	[TagName] [varchar](32) NOT NULL,
	[LogDate] [varchar](12) NOT NULL,
	[LogTime] [varchar](12) NOT NULL,
	[MaxValue] [numeric](18, 6) NULL,
	[AvgValue] [numeric](18, 6) NULL,
	[MinValue] [numeric](18, 6) NULL,
	[LastValue] [numeric](18, 6) NULL,
	[Alarm] [int] NULL,
	[ProductNo] [nvarchar](50) NULL,
	[Production] [nvarchar](50) NULL,
	[Line] [nvarchar](50) NULL,
 CONSTRAINT [PK_BwOpAnalogTable] PRIMARY KEY CLUSTERED 
(
	[BwOpAnalogTableId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Line]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Line](
	[LineId] [int] NOT NULL,
	[LineName] [nvarchar](max) NULL,
	[ItemThroughput] [int] NULL,
	[ScrapRate] [int] NULL,
	[YieldData] [int] NULL,
	[LeadTime] [int] NULL,
	[Status] [int] NULL,
	[StartDateTime] [datetime] NULL,
	[StartedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Line] PRIMARY KEY CLUSTERED 
(
	[LineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ManufacturingTime]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ManufacturingTime](
	[LineId] [int] NOT NULL,
	[PartId] [int] NOT NULL,
	[ManufacturingTime] [int] NULL,
 CONSTRAINT [PK_ManufacturingTime] PRIMARY KEY CLUSTERED 
(
	[LineId] ASC,
	[PartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Motor]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Motor](
	[MotorId] [int] NOT NULL,
	[BeltId] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[SpeedData] [int] NULL,
	[TimeStamp] [datetime] NULL,
	[LastMaintenenceDate] [datetime] NULL,
	[OEEValue] [int] NULL,
 CONSTRAINT [PK_Motor] PRIMARY KEY CLUSTERED 
(
	[MotorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MotorLog]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MotorLog](
	[MotorLogId] [int] NOT NULL,
	[MotorId] [int] NULL,
	[StatType] [nvarchar](max) NULL,
	[LogDateTime] [datetime] NULL,
	[LogValue] [datetime] NULL,
 CONSTRAINT [PK_MotorLog] PRIMARY KEY CLUSTERED 
(
	[MotorLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[OrderId] [int] NOT NULL,
	[PartId] [int] NOT NULL,
	[ProjectName] [nvarchar](max) NULL,
	[LastMaterialDate] [datetime] NULL,
	[ShipDate] [datetime] NULL,
	[Quantity] [int] NULL,
	[Status] [nvarchar](max) NULL,
	[Priority] [nvarchar](max) NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Part]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Part](
	[PartId] [int] NOT NULL,
	[PartName] [nvarchar](max) NULL,
	[Side] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Schedule]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schedule](
	[OrderId] [int] NOT NULL,
	[PartId] [int] NOT NULL,
	[LineId] [int] NOT NULL,
	[BackendId] [int] NULL,
	[BEDate] [datetime] NULL,
	[EarliestStartDate] [datetime] NULL,
	[PlannedStartDate] [datetime] NULL,
	[LatestStartDate] [datetime] NULL,
	[SMTStart] [datetime] NULL,
	[SMTEnd] [datetime] NULL,
 CONSTRAINT [PK_Schedule] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC,
	[PartId] ASC,
	[LineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sensor]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sensor](
	[SensorId] [int] NOT NULL,
	[StationId] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[Data] [int] NULL,
	[TimeStamp] [datetime] NULL,
	[LastMaintenenceDate] [datetime] NULL,
	[SensorType] [nvarchar](max) NULL,
	[OEEValue] [int] NULL,
 CONSTRAINT [PK_Sensor] PRIMARY KEY CLUSTERED 
(
	[SensorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SensorImageLog]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SensorImageLog](
	[SensorImageLogId] [int] NOT NULL,
	[SensorId] [int] NULL,
	[LogDateTime] [datetime] NULL,
	[ImageData] [image] NULL,
 CONSTRAINT [PK_SensorImageLog] PRIMARY KEY CLUSTERED 
(
	[SensorImageLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SensorLog]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SensorLog](
	[SensorLogId] [int] NOT NULL,
	[SensorId] [int] NULL,
	[StatType] [nvarchar](max) NULL,
	[LogDateTime] [datetime] NULL,
	[LogValue] [int] NULL,
 CONSTRAINT [PK_SensorLog] PRIMARY KEY CLUSTERED 
(
	[SensorLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Station]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Station](
	[StationId] [int] NOT NULL,
	[StationName] [nvarchar](max) NULL,
	[LineId] [int] NULL,
	[StatusCodeId] [int] NULL,
	[StatusCodeDescription] [nvarchar](max) NULL,
	[CycleTime] [int] NULL,
	[ChangeOverTime] [int] NULL,
	[Output] [int] NULL,
 CONSTRAINT [PK_Station] PRIMARY KEY CLUSTERED 
(
	[StationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StatusCode]    Script Date: 2/15/2020 4:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StatusCode](
	[StatusCodeId] [int] NOT NULL,
	[StatusCode] [int] NULL,
	[StatusColor] [nvarchar](max) NULL,
 CONSTRAINT [PK_StatusCode] PRIMARY KEY CLUSTERED 
(
	[StatusCodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [SIMULATION] SET  READ_WRITE 
GO
