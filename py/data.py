# ----------------------
# DATA SIMULATION SCRIPT (BwAnaChgLog)
# ----------------------
# This script will simulate real-time incoming data and push it into BwAnaChgLog.
# This data will then be processed by the Log Filter Service and pushed to BwOpAnaChgLog if line is in operation.
# ----------------------
# SIT UOG AY 19/20 FYP
# A DIGITAL OPERATION DASHBOARD FOR VENTURE
# CHAN TING FENG, TIM
# 2427227C / 1800853
# ----------------------
# DATABASE CONFIGURATION
# ----------------------
DB_DRIVER = '{ODBC Driver 17 for SQL Server}'
SERVER = '127.0.0.1'
DB_NAME = 'ITSCADA'
USERNAME = 'sa'
PASSWORD = 'venture'
# ----------------------

import pyodbc
from datetime import datetime as dt
from time import sleep
import random
import multiprocessing

connectString = 'Driver='+DB_DRIVER+';Server='+SERVER+';Database='+DB_NAME+';UID='+USERNAME+';PWD='+PASSWORD+';Trusted_Connection=yes;'

def bwAnaChgLog(tagName):
    while(1):
        conn = pyodbc.connect(connectString)
        cursor = conn.cursor()
        currentDateTime = dt.now()
        if (tagName.endswith("Target")):
            value = 65 # default random target number for each line
        else:
            value = random.randrange(50,60)
        query = "INSERT INTO dbo.BwAnaChgLog (ProjNodeId, TagName, LogDate, LogTime, LogMilliSecond, LogValue, Alarm) VALUES (?,?,?,?,?,?,?)"
        val = [1, tagName, currentDateTime.strftime("%y/%m/%d"), currentDateTime.strftime("%H:%M:%S"), random.randrange(800,995), value, 0]
        cursor.execute(query, val)
        conn.commit()
        print('Added to BwAnaChgLog: ', val)
        sleep_sec = random.randrange(10,50)
        sleep(sleep_sec)

def bwAnaChgLog_output(tagName, start_from_zero):
    while(1):
        if (start_from_zero != True):
            conn = pyodbc.connect(connectString)
            cursor = conn.cursor()
            query_get = "SELECT TOP(1) LogValue FROM dbo.BwAnaChgLog WHERE tagName=? ORDER BY LogDate DESC, LogTime DESC"
            val_get = [tagName]
            cursor.execute(query_get, val_get)
            row = cursor.fetchone()
            if row:
                value = row[0]
            value += 1
        else:
             value = 0

        conn = pyodbc.connect(connectString)
        cursor = conn.cursor()
        currentDateTime = dt.now()
        query = "INSERT INTO dbo.BwAnaChgLog (ProjNodeId, TagName, LogDate, LogTime, LogMilliSecond, LogValue, Alarm) VALUES (?,?,?,?,?,?,?)"
        val = [1, tagName, currentDateTime.strftime("%y/%m/%d"), currentDateTime.strftime("%H:%M:%S"), random.randrange(800,995), value, 0]
        cursor.execute(query, val)
        conn.commit()
        print('Added to BwAnaChgLog: ', val)
        start_from_zero = False
        sleep_sec = random.randrange(10,20)
        sleep(sleep_sec)


if __name__ == '__main__':

    # data change output
    p1 = multiprocessing.Process(target=bwAnaChgLog_output, args=['SMT_Line_7_Output', True])
    p2 = multiprocessing.Process(target=bwAnaChgLog_output, args=['SMT_Line_8_Output', True])
    p3 = multiprocessing.Process(target=bwAnaChgLog_output, args=['SMT_Line_9_Output', True])
    p4 = multiprocessing.Process(target=bwAnaChgLog_output, args=['SMT_Line_10_Output', True])
    p5 = multiprocessing.Process(target=bwAnaChgLog_output, args=['SMT_Line_11_Output', True])
    p6 = multiprocessing.Process(target=bwAnaChgLog_output, args=['SMT_Line_12_Output', True])

    # data change target
    p7 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_7_Target'])
    p8 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_8_Target'])
    p9 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_9_Target'])
    p10 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_10_Target'])
    p11 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_11_Target'])
    p12 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_12_Target'])

    # data change temperature
    p13 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_7_Product_Temper'])
    p14 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_8_Product_Temper'])
    p15 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_9_Product_Temper'])
    p16 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_10_Product_Temper'])
    p17 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_11_Product_Temper'])
    p18 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_12_Product_Temper'])
    

    # data change humidity
    p19 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_7_Product_Humidity'])
    p20 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_8_Product_Humidity'])
    p21 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_9_Product_Humidity'])
    p22 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_10_Product_Humidity'])
    p23 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_11_Product_Humidity'])
    p24 = multiprocessing.Process(target=bwAnaChgLog, args=['SMT_Line_12_Product_Humidity'])
    
    
    # hourly data output
    # p25 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_7_Output'])
    # p26 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_8_Output'])
    # p27 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_9_Output'])
    # p28 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_10_Output'])
    # p29 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_11_Output'])
    # p30 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_12_Output'])
    
    # hourly data target
    # p31 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_7_Target'])
    # p32 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_8_Target'])
    # p33 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_9_Target'])
    # p34 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_10_Target'])
    # p35 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_11_Target'])
    # p36 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_12_Target'])

    # hourly data temperature
    # p37 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_7_Product_Temper'])
    # p38 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_8_Product_Temper'])
    # p39 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_9_Product_Temper'])
    # p40 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_10_Product_Temper'])
    # p41 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_11_Product_Temper'])
    # p42 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_12_Product_Temper'])

    # hourly data humidity
    # p43 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_7_Product_Humidity'])
    # p44 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_8_Product_Humidity'])
    # p45 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_9_Product_Humidity'])
    # p46 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_10_Product_Humidity'])
    # p47 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_11_Product_Humidity'])
    # p48 = multiprocessing.Process(target=bwAnalogTable, args=['SMT_Line_12_Product_Humidity'])

    p1.start()
    p2.start()
    p3.start()
    # p4.start()
    # p5.start()
    p6.start()
    p7.start()
    p8.start()
    p9.start()
    # p10.start()
    # p11.start()
    p12.start()
    p13.start()
    p14.start()
    p15.start()
    # p16.start()
    # p17.start()
    p18.start()
    p19.start()
    p20.start()
    p21.start()
    # p22.start()
    # p23.start()
    p24.start()
    # p25.start()
    # p26.start()
    # p27.start()
    # p28.start()
    # p29.start()
    # p30.start()
    # p31.start()
    # p32.start()
    # p33.start()
    # p34.start()
    # p35.start()
    # p36.start()
    # p37.start()
    # p38.start()
    # p39.start()
    # p40.start()
    # p41.start()
    # p42.start()
    # p43.start()
    # p44.start()
    # p45.start()
    # p46.start()
    # p47.start()
    # p48.start()