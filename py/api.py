# ----------------------
# CHART RETRIEVAL SERVICE
# ----------------------
# This script provides the retrieval of additional charts using other charting frameworks.
# As of the current implementation, only the timeline chart for the production schedule requires the use of a non-Grafana visualization.
# ----------------------
# SIT UOG AY 19/20 FYP
# A DIGITAL OPERATION DASHBOARD FOR VENTURE
# CHAN TING FENG, TIM
# 2427227C / 1800853
# ----------------------
# DATABASE CONFIGURATION (ventureDB - PHYSICAL)
# ----------------------
DB_DRIVER = '{ODBC Driver 17 for SQL Server}'
SERVER = '127.0.0.1'
DB_NAME = 'GRAFANA'
USERNAME = 'sa'
PASSWORD = 'venture'
# ----------------------
# DATABASE CONFIGURATION (simulationDB - SIMULATION)
# ----------------------
S_DB_DRIVER = '{ODBC Driver 17 for SQL Server}'
S_SERVER = '127.0.0.1'
S_DB_NAME = 'SIMULATION'
S_USERNAME = 'sa'
S_PASSWORD = 'venture'
# ----------------------
# MISC SETTINGS
# ----------------------
PRODUCT_COLORS_BLACKLIST = ['#d44a3a', '#eab839', '#56a64b', '#ecb1aa', '#96d98d', '#de7569']
# ----------------------

import pyodbc
from flask import Flask, request
from flask_restful import Resource, Api
from json import dumps
from flask import jsonify
from flask import Response
from flask import request
from datetime import datetime
import datetime as dt
import random

connectString = ""
cS = 1
pConnectString = 'Driver='+DB_DRIVER+';Server='+SERVER+';Database='+DB_NAME+';UID='+USERNAME+';PWD='+PASSWORD+';Trusted_Connection=yes;'
sConnectString = 'Driver='+S_DB_DRIVER+';Server='+S_SERVER+';Database='+S_DB_NAME+';UID='+S_USERNAME+';PWD='+S_PASSWORD+';Trusted_Connection=yes;'
app = Flask(__name__)
api = Api(app)
productColors = []

def randColor():
    random_number = random.randint(0,16777215)
    hex_number = format(random_number,'x')
    hex_number = '#' + hex_number
    for color in PRODUCT_COLORS_BLACKLIST:
        if (hex_number == color):
            hex_number = randColor()
            # break
    for color in productColors:
        if (hex_number == color):
            hex_number = randColor()    
    return hex_number

def setProductColors():
    if (cS == 1):
        connectString = pConnectString
    else:
        connectString = sConnectString
    conn = pyodbc.connect(connectString)
    cursor = conn.cursor()
    query = "SELECT COUNT(*) FROM Part"
    cursor.execute(query)
    count = cursor.fetchone()
    for _ in range(count[0]):
        productColors.append(randColor())

def getOrderDetails(orderId):
    if (cS == 1):
        connectString = pConnectString
    else:
        connectString = sConnectString
    conn = pyodbc.connect(connectString)
    cursor = conn.cursor()
    query = "SELECT ProjectName, Quantity, LastMaterialDate, ShipDate, Status, Priority FROM [Order] WHERE OrderId=?"
    val = [orderId]
    cursor.execute(query, val)
    orderName = cursor.fetchone()
    return orderName

def getLineName(lineId):
    if (cS == 1):
        connectString = pConnectString
    else:
        connectString = sConnectString
    conn = pyodbc.connect(connectString)
    cursor = conn.cursor()
    query = "SELECT LineName FROM Line WHERE LineId=?"
    val = [lineId]
    cursor.execute(query, val)
    lineName = cursor.fetchone()
    return lineName[0]

def getProductDetails(partId):
    if (cS == 1):
        connectString = pConnectString
    else:
        connectString = sConnectString
    conn = pyodbc.connect(connectString)
    cursor = conn.cursor()
    query = "SELECT PartName, Side, PartId FROM Part WHERE PartId=?"
    val = [partId]
    cursor.execute(query, val)
    productDetails = cursor.fetchone()
    return productDetails

@app.route('/getSchedule', methods=['GET'])
def getSchedule():
    # read in arguments (date time range, color scheme by, or work order search query)
    args = request.args
    fromDateTime = int(args['from'])
    toDateTime = int(args['to'])
    colorSchemeBy = str(args['color'])
    searchQuery = str(args['search'])
    viewNowRef = str(args['ref'])
    dataSource = str(args['db'])

    # check selected data source
    if (dataSource == 'ventureDB'):
        cS = 1
    else:
        cS = 2

    data = []
    if (cS == 1):
        connectString = pConnectString
    else:
        connectString = sConnectString
    conn = pyodbc.connect(connectString)
    cursor = conn.cursor()

    # if search query is not null, look for work order in db
    if (searchQuery != ''):
        query = "SELECT * FROM Schedule WHERE OrderId = (SELECT OrderId FROM [Order] WHERE ProjectName=?)"
        val = [searchQuery]
    else: # if not search based on selected date time range

        # date_time_range_diff = fromDateTime - toDateTime
        # if (date_time_range_diff < (6 * 60 * 60 * 1000))
        #     fromDateTime = toDateTime - (6 * 60 * 60 * 1000)

        # convert to string format (for SQL query)
        convertedFromDateTime = datetime.utcfromtimestamp(fromDateTime/1000).strftime('%Y-%m-%d %H:%M:%S')
        convertedToDateTime = datetime.utcfromtimestamp(toDateTime/1000).strftime('%Y-%m-%d %H:%M:%S')
        print(convertedFromDateTime + ' - ' + convertedToDateTime)

        query = "SELECT * FROM Schedule WHERE SMTStart <= CAST(DATEADD(HOUR, 8, ?) AS datetime2) AND SMTEnd >= CAST(DATEADD(HOUR, 8, ?) AS datetime2) Order By LineId"
        val = [convertedToDateTime, convertedFromDateTime]

    cursor.execute(query, val)
    rows = cursor.fetchall()
    for row in rows:
        data.append(row)

    # error handling: check if there are any entries for selected date time range or search query
    if (len(data) == 0):
        html = """
        <html>
        <style>
        h3, p {
            font-family: Arial, Helvetica, sans-serif;
        }
        </style>
        <center>
        <h3>No Scheduled Work Orders Found</h3>
        <p>No scheduled work orders were found based on your search query or between the selected date and time range. Please recheck your search query or select another date time range!</p>
        </center>
        </html>
        """
    else:
        rowCount = 0
        addRows = "dataTable.addRows(["
        
        utc_sg_time_diff = dt.timedelta(hours=8) # UTC to SG time difference is 8 hrs
        nowDT = datetime.now().replace(microsecond=0) # get current date time as DateTime object(without microseconds)

        # get selected date time range dates + 8hrs time difference in DateTime format
        fromDT = (datetime.utcfromtimestamp(fromDateTime/1000) + utc_sg_time_diff).replace(microsecond=0)
        toDT = ((datetime.utcfromtimestamp(toDateTime/1000) + utc_sg_time_diff).replace(microsecond=0))

        # check if current date time exists in between selected date time range
        # if (nowDT >= fromDT and nowDT <= toDT):
        if (viewNowRef == 'Yes'):
            addRows += "[dateString, 'Now', '', '#d54', new Date(), new Date()]," # include 'Now' reference line in schedule

        # if search query is not null, set hAxis max and min values to retrieved work order's start and end date
        if (searchQuery != ''): 
            hAxisMin = "new Date("+str(data[0][8].year)+", "+str(data[0][8].month-1)+", "+str(data[0][8].day)+", "+str(data[0][8].hour)+", "+str(data[0][8].minute)+", "+str(data[0][8].second)+")"
            hAxisMax = "new Date("+str(data[0][9].year)+", "+str(data[0][9].month-1)+", "+str(data[0][9].day)+", "+str(data[0][9].hour)+", "+str(data[0][9].minute)+", "+str(data[0][9].second)+")"
        else: # otherwise set to selected date time range
            hAxisMin = "new Date("+str(fromDT.year)+", "+str(fromDT.month-1)+", "+str(fromDT.day)+","+str(fromDT.hour)+", "+str(fromDT.minute)+", "+str(fromDT.second)+")"
            hAxisMax = "new Date("+str(toDT.year)+", "+str(toDT.month-1)+", "+str(toDT.day)+","+str(toDT.hour)+", "+str(toDT.minute)+", "+str(toDT.second)+")"

        for row in data:
            rowCount += 1

            # process start and end date for each scheduled work order
            startDate = "new Date("+str(row[8].year)+", "+str(row[8].month-1)+", "+str(row[8].day)+", "+str(row[8].hour)+", "+str(row[8].minute)+", "+str(row[8].second)+")"
            endDate = "new Date("+str(row[9].year)+", "+str(row[9].month-1)+", "+str(row[9].day)+", "+str(row[9].hour)+", "+str(row[9].minute)+", "+str(row[9].second)+")"

            # get details for scheduled work order and product
            orderDetails = getOrderDetails(row[0])
            productDetails = getProductDetails(row[1])

            # process priority's tooltip for each scheduled work order
            priority_content = int(orderDetails[5])
            if (priority_content == 1):
                priority = '''<b style="color:#d44a3a;">High</b>'''
            elif (priority_content == 2):
                priority = '''<b style="color:#eab839;">Medium</b>'''  
            else: # normal priority
                priority = '''<b style="color:#56a64b;">Normal</b>'''
            
            # process status' tooltip + alert message for each scheduled work order
            status_content = str(orderDetails[4])
            alert = ""
            if (status_content == 'Processing'):
                status = '''<b style="color:#56a64b;">'''+status_content+'''</b>'''
                if (nowDT > row[9]): # if current date time is later than SMTEnd
                    status = '''<b style="color:#d44a3a;">'''+status_content+'''</b>'''
                    alert += '''<h4 style="color:#d44a3a;">Critical: Work order is still processing!</h4><hr/>'''
                if (row[8] > row[7]): # if work order scheduled to ship late (SMTStart > LatestStartDate)
                    status = '''<b style="color:#de7569;">'''+status_content+'''</b>'''
                    alert += '''<h4 style="color:#de7569;">This work order will be shipped late!</h4><hr/>'''
            elif (status_content == 'Scheduled'):
                status = '''<b style="color:#96d98d;">'''+status_content+'''</b>'''
                if (nowDT > row[8]): # if current date time is later than SMTStart
                    status = '''<b style="color:#d44a3a;">'''+status_content+'''</b>'''
                    alert += '''<h4 style="color:#d44a3a;">Critical: Work order has not started <br/>processing!</h4><hr/>'''
                if (row[8] > row[7]): # if work order scheduled to ship late (SMTStart > LatestStartDate)
                    status = '''<b style="color:#eab839;">'''+status_content+'''</b>'''
                    alert += '''<h4 style="color:#eab839;">This work order is scheduled to be <br/>shipped late!</h4><hr/>'''
            else: # completed
                status = '''<b style="color:#56a64b;">'''+status_content+'''</b>'''
                if (row[8] > row[7]):
                    alert += '''<h4 style="color:#ecb1aa;">This work order was shipped late!</h4><hr/>''' 
                    status = '''<b style="color:#ecb1aa;">'''+status_content+'''</b>''' # d44a3a

            # process product details' tooltip
            product = str(productDetails[0]) + ' (' + str(productDetails[1]) + ')'

            # process timeline color scheme based on selected option
            if (colorSchemeBy == 'Status'):
                if (status_content == 'Processing'):
                    color = "'#5a5'" # medium sea green
                    if (row[8] > row[7]): # if work order scheduled to ship late (SMTStart > LatestStartDate)
                        color = "'#e77'" # light coral
                    if (nowDT > row[9]): # if current date time is later than SMTEnd
                        color = "'#d54'" # red
                elif (status_content == 'Scheduled'):
                    color = "'#9e9'" # light green
                    if (row[8] > row[7]): # if work order scheduled to ship late (SMTStart > LatestStartDate)
                        color = "'#fc4'" # yellow
                    if (nowDT > row[8]): # if current date time is later than SMTStart
                        color = "'#d54'" # indian red    
                else: # completed
                    color = "null" # grey
                    if (row[8] > row[7]):
                        color = "'#fbb'" # light pink
            elif (colorSchemeBy == 'Priority'):
                if (priority_content == 1):
                    color = "'#d54'" # indian red
                elif (priority_content == 2):
                    color = "'#fc4'" # golden rod
                else:
                    color = "'#5a5'" #medium sea green
            elif (colorSchemeBy == 'ProductNo'):
                productId = int(productDetails[2])
                hexColor = str(productColors[productId-1])
                color = "'" + hexColor + "'"
                # assign color to tooltip as well
                product = '''<b style="color:''' + hexColor + ''';">''' + str(productDetails[0]) + ''' (''' + str(productDetails[1]) + ''')</b>'''
            
            # process tooltip
            tooltip = "'"+alert+"<p><b>Work Order No: </b>"+str(orderDetails[0])+"<br/><b>Product No: </b>"+product+"<br/><b>Quantity:</b> "+str(orderDetails[1])+"<br/><b>Last Material Date:</b> "+orderDetails[2].strftime('%Y-%m-%d %H:%M')+"<br/><b>Ship Date:</b> "+orderDetails[3].strftime('%Y-%m-%d %H:%M')+"<br/><b>Status:</b> "+status+"<br/><b>Priority:</b> "+priority+"<p><hr/><p><b>Backend Date:</b> "+row[4].strftime('%Y-%m-%d %H:%M')+"<br/><b>Earliest Start Date:</b> "+row[5].strftime('%Y-%m-%d %H:%M')+"<br/><b>Planned Start Date:</b> "+row[6].strftime('%Y-%m-%d %H:%M')+"<br/><b>Latest Start Date:</b> "+row[7].strftime('%Y-%m-%d %H:%M')+"</p><hr><p><b>Line:</b> "+str(getLineName(row[2]))+"<br/><b>SMT Start:</b> "+row[8].strftime('%Y-%m-%d %H:%M')+"<br/><b>SMT End:</b> "+row[9].strftime('%Y-%m-%d %H:%M')+"</p>'"
            
            # add processed scheduled work order + tooltip details to variable in javascript format readable by Google Charts
            addRows += "['"+str(getLineName(row[2]))+"', '"+str(orderDetails[0])+"', "+tooltip+", "+color+", "+startDate+", "+endDate+"]"
            
            # if not last entry, add a comma
            if (rowCount < len(data)):
                addRows += ","
        addRows += "]);"

        html = """
        <html>
        <head>
        <script src="https://www.gstatic.com/charts/loader.js"></script>
        <script src="https://code.jquery.com/jquery-2.1.3.js" integrity="sha256-goy7ystDD5xbXSf+kwL4eV6zOPJCEBD1FBiCElIm+U8=" crossorigin="anonymous"></script>
        <script type="text/javascript">
        google.charts.load('current', {'packages':['timeline']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var container = document.getElementById('timeline');
            var chart = new google.visualization.Timeline(container);
            var dataTable = new google.visualization.DataTable();
            var today = new Date();
            var dateString =
            today.getFullYear() + "/" +
                ("0" + (today.getMonth()+1)).slice(-2) + "/" +
                ("0" + today.getDate()).slice(-2) + " " +
                ("0" + today.getHours()).slice(-2) + ":" +
                ("0" + today.getMinutes()).slice(-2) + ":" +
                ("0" + today.getSeconds()).slice(-2);

            var options = {
                timeline: { singleColor: '#777' },
                tooltip: { isHtml: true },
                hAxis: {
                    minValue: """ + hAxisMin + """,
                    maxValue: """ + hAxisMax + """
                },
                avoidOverlappingGridLines: true,
            };
            
            dataTable.addColumn({ type: 'string', id: 'Line' });
            dataTable.addColumn({ type: 'string', id: 'Name' });
            dataTable.addColumn({ type: 'string', role: 'tooltip', 'p': {'html': true}});
            dataTable.addColumn({ type: 'string', role: 'style'})
            dataTable.addColumn({ type: 'date', id: 'Start' });
            dataTable.addColumn({ type: 'date', id: 'End' });
            
            """ + addRows + """
            
            nowLine('timeline');
            
            var showRef = getUrlVars()["ref"];
            google.visualization.events.addListener(chart, 'onmouseover', function(obj){
                if(obj.row == 0 && showRef == 'Yes'){
                    $('.google-visualization-tooltip').css('display', 'none');
                    }
                nowLine('timeline');
            })
            
            google.visualization.events.addListener(chart, 'onmouseout', function(obj){
                nowLine('timeline');
            })

            var observer = new MutationObserver(SetBorderStyle);
            google.visualization.events.addListener(chart, 'ready', function () {
                nowLine('timeline');
                SetBorderStyle();
                observer.observe(container, {
                childList: true,
                subtree: true
                });
            });

            function SetBorderStyle() {
                Array.prototype.forEach.call(container.getElementsByTagName('rect'), function (rect) {
                if (parseFloat(rect.getAttribute('height')) < 23) {
                    rect.setAttribute('stroke', 'black')
                    rect.setAttribute('stroke-width', 3)
                    rect.setAttribute('stroke-opacity', 0.1)
                    rect.setAttribute('rx', 5);
                    rect.setAttribute('ry', 5);
                }
                if (parseFloat(rect.getAttribute('width')) > 9) { }
                });
            }

            chart.draw(dataTable, options);
        }

        function nowLine(div){
            var height;
            $('#' + div + ' rect').each(function(index){
                var x = parseFloat($(this).attr('x'));
                var y = parseFloat($(this).attr('y'));
            
                if(x == 0 && y == 0) {height = parseFloat($(this).attr('height'))}
            })

            var nowWord = $('#' + div + ' text:contains("Now")');
        
            nowWord.prev().first().attr('height', height + 'px').attr('width', '1px').attr('y', '0');

            $('#' + div + '  text:contains("Now")').each(function(idx, value) {
                if (idx == 0) {
                    $(value).parent().find("rect").first().removeAttr("style");
                } else if (idx == 1) {
                    $(value).parent().find("rect").first().attr("style", "display:none;");
                }
            });
        }
        
        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }
        </script>
        <style>
        p {
            padding: 0px 10px 0px 10px;
            font-family: Arial, Helvetica, sans-serif;
        }
        h4 {
            font-family: Arial, Helvetica, sans-serif;
            // color: #d44a3a;
            text-align: center;
        }
        </style>
        </head>
        <body>
        <div id="timeline" style="height:350px;"></div>
        </body>
        </html>
        """

    resp = Response(html, status=200, mimetype='text/html')
    return resp

if __name__ == '__main__':
    setProductColors() # define product colors
    app.run(port='5002')