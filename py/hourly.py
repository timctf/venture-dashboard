# ----------------------
# DATA SIMULATION SCRIPT (BwOpAnalogTable)
# ----------------------
# This script will simulate data for every past hours of the day til the current time and push it into BwOpAnalogTable.
# ----------------------
# SIT UOG AY 19/20 FYP
# A DIGITAL OPERATION DASHBOARD FOR VENTURE
# CHAN TING FENG, TIM
# 2427227C / 1800853
# ----------------------
# DATABASE CONFIGURATION
# ----------------------
DB_DRIVER = '{ODBC Driver 17 for SQL Server}'
SERVER = '127.0.0.1'
DB_NAME = 'GRAFANA'
USERNAME = 'sa'
PASSWORD = 'venture'
# ----------------------

import pyodbc
import random
from datetime import datetime as dt

connectString = 'Driver='+DB_DRIVER+';Server='+SERVER+';Database='+DB_NAME+';UID='+USERNAME+';PWD='+PASSWORD+';Trusted_Connection=yes;'

def reset():
    # delete exisiting entries for the day - to reset
    conn = pyodbc.connect(connectString)
    cursor = conn.cursor()
    query = 'DELETE FROM BwOpAnalogTable WHERE LogDate = ?'
    val = [dt.now().strftime("%y/%m/%d")]
    cursor.execute(query, val)
    conn.commit()
    print('Previous data for the day deleted.')

def bwAnalogTable(tagName, line):
    currentDateTime = dt.now()
    currentHour = currentDateTime.hour
    hourCount = 0
    for _ in range(currentHour+1):
        conn = pyodbc.connect(connectString)
        cursor = conn.cursor()
        if (tagName.endswith("Output")):
            productNo = 'QFBo10615E'
        else:
            productNo = ''
        if (tagName.endswith("Target")):
            minValue = 65 # default random target number for each line
            maxValue = 65
            lastValue = 65
            avgValue = 65
        else:
            minValue = random.randrange(40,52)
            maxValue = random.randrange(58,70)
            lastValue = random.randrange(50,60)
            avgValue = random.randrange(50,60)
        query = "INSERT INTO dbo.BwOpAnalogTable (ProjNodeId, TagName, LogDate, LogTime, MaxValue, AvgValue, MinValue, LastValue, Alarm, ProductNo, Production, Line) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"
        val = [1, tagName, currentDateTime.strftime("%y/%m/%d"), str(hourCount).zfill(2)+":00:00", maxValue, avgValue, minValue, lastValue, 0, productNo, 'SMT', line]
        cursor.execute(query, val)
        conn.commit()
        print('Added to BwOpAnalogTable: ', val)
        hourCount += 1

if __name__ == '__main__':
    reset()

    bwAnalogTable("SMT_Line_7_Output", "SMT_Line_7")
    bwAnalogTable("SMT_Line_8_Output", "SMT_Line_8")
    bwAnalogTable("SMT_Line_9_Output", "SMT_Line_9")
    bwAnalogTable("SMT_Line_12_Output", "SMT_Line_12")

    bwAnalogTable("SMT_Line_7_Target", "SMT_Line_7")
    bwAnalogTable("SMT_Line_8_Target", "SMT_Line_8")
    bwAnalogTable("SMT_Line_9_Target", "SMT_Line_9")
    bwAnalogTable("SMT_Line_12_Target", "SMT_Line_12")

    bwAnalogTable("SMT_Line_7_Product_Temper", "SMT_Line_7")
    bwAnalogTable("SMT_Line_8_Product_Temper", "SMT_Line_8")
    bwAnalogTable("SMT_Line_9_Product_Temper", "SMT_Line_9")
    bwAnalogTable("SMT_Line_12_Product_Temper", "SMT_Line_12")

    bwAnalogTable("SMT_Line_7_Product_Humidity", "SMT_Line_7")
    bwAnalogTable("SMT_Line_8_Product_Humidity", "SMT_Line_8")
    bwAnalogTable("SMT_Line_9_Product_Humidity", "SMT_Line_9")
    bwAnalogTable("SMT_Line_12_Product_Humidity", "SMT_Line_12")