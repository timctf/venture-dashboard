# ----------------------
# LOG FILTER SERVICE
# ----------------------
# This script provides the transfer of data whenever a production line is in operation.
# Tranfer of data is from BwAnaChgLog and BwAnalogTable to BwOpAnaChgLog and BwOpAnalogTable.
# ----------------------
# SIT UOG AY 19/20 FYP
# A DIGITAL OPERATION DASHBOARD FOR VENTURE
# CHAN TING FENG, TIM
# 2427227C / 1800853
# ----------------------
# DATABASE CONFIGURATION
# ----------------------
FROM_DB_DRIVER = '{ODBC Driver 17 for SQL Server}'
FROM_SERVER = '127.0.0.1'
FROM_DB_NAME = 'ITSCADA'
FROM_USERNAME = 'sa'
FROM_PASSWORD = 'venture'
# ----------------------
TO_DB_DRIVER = '{ODBC Driver 17 for SQL Server}'
TO_SERVER = '127.0.0.1'
TO_DB_NAME = 'GRAFANA'
TO_USERNAME = 'sa'
TO_PASSWORD = 'venture'
# ----------------------
# MISC SETTINGS
# ----------------------
BWANACHGLOG_IDLE_TIME = 0.5 # in seconds
BWANALOGTABLE_IDLE_TIME = 60 # in seconds (3600 seconds on default - checks only every hour)
TRANSFER_PROCESS_IDLE_TIME = 0.5 # in seconds
TRANSFER_PROCESSES = 2 # no. of processes to spawn for handling TRANSFER of logged data
# ----------------------
# IMPORTANT INFORMATION
# ----------------------
# Type variable:
# 0 - referencing data transfer from 'BwAnaChgLog' to 'BwOpAnaChgLog'
# 1 - referencing data transfer from 'BwAnalogTable' to 'BwOpAnalogTable'
# ----------------------

import pyodbc
from datetime import datetime as dt
from time import sleep
import multiprocessing

fromConnectString = 'Driver='+FROM_DB_DRIVER+';Server='+FROM_SERVER+';Database='+FROM_DB_NAME+';UID='+FROM_USERNAME+';PWD='+FROM_PASSWORD+';Trusted_Connection=yes;'
toConnectString = 'Driver='+TO_DB_DRIVER+';Server='+TO_SERVER+';Database='+TO_DB_NAME+';UID='+TO_USERNAME+';PWD='+TO_PASSWORD+';Trusted_Connection=yes;'

def check_line_info(tagName, conn):
    cursor = conn.cursor()
    query = "SELECT product_no, production, line, line_status FROM dbo.Target WHERE line IN (SELECT line FROM dbo.ConfigTable WHERE tagname=?)"
    val = [tagName]
    cursor.execute(query, val)
    row = cursor.fetchone()
    cursor.close()
    del cursor
    return row

def count_rows(type, conn):
    cursor = conn.cursor()
    if (type == 0):
        query = "SELECT COUNT(*) FROM dbo.BwAnaChgLog WHERE TagName IN (SELECT tagname FROM dbo.ConfigTable)"
    elif (type == 1):
        query = "SELECT COUNT(*) FROM dbo.BwAnalogTable WHERE TagName IN (SELECT tagname FROM dbo.ConfigTable)"
    cursor.execute(query)
    row = cursor.fetchone()
    if row:
        rowCount = row[0]
    cursor.close()
    del cursor
    return rowCount

def check_io(tagName, conn):
    cursor = conn.cursor()
    query = "SELECT io FROM dbo.ConfigTable WHERE tagname=?"
    val = [tagName]
    cursor.execute(query, val)
    row = cursor.fetchone()
    if row:
        io = row[0]
    cursor.close()
    del cursor
    return io

def get_new_data(type, q):
    connFrom = pyodbc.connect(fromConnectString)
    row_count = count_rows(type, connFrom)
    connFrom.close()
    # if (type == 1):
    #     sleep((61 - dt.now().minute) * 60) # slack time to cater for when script is initiated 
    while(1):
        connFrom = pyodbc.connect(fromConnectString)
        cursor = connFrom.cursor()
        if (type == 0):
            query = "SELECT * FROM dbo.BwAnaChgLog WHERE TagName IN (SELECT tagname FROM dbo.ConfigTable) ORDER BY LogDate, LogTime OFFSET ? ROWS"
        elif (type == 1):
            query = "SELECT * FROM dbo.BwAnalogTable WHERE TagName IN (SELECT tagname FROM dbo.ConfigTable) ORDER BY LogDate, LogTime OFFSET ? ROWS"
        val = [row_count]
        cursor.execute(query, val)
        rows = cursor.fetchall()
        for row in rows:
            row_count += 1
            tagName = row[1]
            lineInfo = check_line_info(tagName, connFrom)
            if (lineInfo[3] == 1): # check if line is in operation
                # dont include ProductNo if tagName is output
                if (check_io(tagName, connFrom) != 'Output'):
                    lineInfo[0] = '' 
                # add to jobQueue
                q.put([type, row, lineInfo[0], lineInfo[1], lineInfo[2]])
                # print("Added to jobQueue: ", row)
        cursor.close()
        del cursor
        connFrom.close()
        if (type == 0):
            sleep(BWANACHGLOG_IDLE_TIME)
        elif (type == 1):
            sleep(BWANALOGTABLE_IDLE_TIME)

def transfer_data(q):
    while(1):
        if (q.empty() != 1):
            dataRow = q.get()
            connTo = pyodbc.connect(toConnectString)
            cursor = connTo.cursor()
            # if (dataRow[2] != ''):
            #     dataRow[1][1] = dataRow[2]
            if (dataRow[0] == 0):
                table = 'BwOpAnaChgLog'
                query = "INSERT INTO dbo.BwOpAnaChgLog (ProjNodeId, TagName, LogDate, LogTime, LogMilliSecond, LogValue, Alarm, ProductNo, Production, Line) VALUES (?,?,?,?,?,?,?,?,?,?)"
                val = [str(dataRow[1][0]), str(dataRow[1][1]), str(dataRow[1][2]), str(dataRow[1][3]), str(dataRow[1][4]), str(dataRow[1][5]), str(dataRow[1][6]), str(dataRow[2]), str(dataRow[3]), str(dataRow[4])]
            elif (dataRow[0] == 1):
                table = 'BwOpAnalogTable'
                query = "INSERT INTO dbo.BwOpAnalogTable (ProjNodeId, TagName, LogDate, LogTime, MaxValue, AvgValue, MinValue, LastValue, Alarm, ProductNo, Production, Line) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"
                val = [str(dataRow[1][0]), str(dataRow[1][1]), str(dataRow[1][2]), str(dataRow[1][3]), str(dataRow[1][4]), str(dataRow[1][5]), str(dataRow[1][6]), str(dataRow[1][7]), str(dataRow[1][8]), str(dataRow[2]), str(dataRow[3]), str(dataRow[4])]
            cursor.execute(query, val)
            connTo.commit()
            print("Copied to " + table + ": ", val)
            cursor.close()
            del cursor
            connTo.close()
        sleep(TRANSFER_PROCESS_IDLE_TIME)

if __name__ == '__main__':
    jobQueue = multiprocessing.Queue()

    gA = multiprocessing.Process(target=get_new_data, args=[0,jobQueue]) # BwAnaChgLog
    gR = multiprocessing.Process(target=get_new_data, args=[1,jobQueue]) # BwAnalogTable
    gA.start()
    gR.start()

    for _ in range(TRANSFER_PROCESSES):
        t = multiprocessing.Process(target=transfer_data, args=[jobQueue])
        t.start()